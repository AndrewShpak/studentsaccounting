using System;
using System.Collections.Generic;

namespace StudentsAccounting.Domain.Entities
{
    public class Role
    {
        public string Id { get;  set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public ICollection<UserRole> UserRoles { get;  set; } = new HashSet<UserRole>();
    }
}