using System;
using System.Collections.Generic;

namespace StudentsAccounting.Domain.Entities
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }

        public string OwnerId { get; set; }
        public User Owner { get; set; }
        public DateTime Registered { get;  set; } = DateTime.Now;

        public ICollection<UserCourse> UserCourses { get;  set; } = new HashSet<UserCourse>();
    }
}