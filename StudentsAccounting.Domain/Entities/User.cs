using System;
using System.Collections.Generic;

namespace StudentsAccounting.Domain.Entities
{
    public class User
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public int Age { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime Registered { get; set; } = DateTime.Now;

        public DateTime? StudyDate { get; set; }
        
        public string FullName => $"{Name} {LastName}";

        public ICollection<UserCourse> UserCourses { get; set; } = new HashSet<UserCourse>();

        public ICollection<UserRole> UserRoles { get; set; } = new HashSet<UserRole>();

        public string Avatar { get; set; }

        public ICollection<Course> Courses { get; set; }
            = new HashSet<Course>();
    }
}