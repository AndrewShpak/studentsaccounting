using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;
using StudentsAccounting.Application.Interfaces.Email;

namespace StudentsAccounting.Infrastructure
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Send(Message message)
        {
            var emailMessage = CreateMessage(message);
            var emailAuthentication = _configuration.GetSection("EmailAuthentication");
            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 465, true);
                client.Authenticate(emailAuthentication["Email"], emailAuthentication["Password"]);
                client.Send(emailMessage);
                client.Disconnect(true);
            }
        }

        private static MimeMessage CreateMessage(Message message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Students Accounting Service", "rozklademail@oa.edu.ua"));
            emailMessage.To.Add(new MailboxAddress(message.ToUserName, message.ToEmail));
            emailMessage.Subject = message.Subject;
            emailMessage.Body = new TextPart(TextFormat.Plain)
            {
                Text = message.Body
            };
            return emailMessage;
        }
    }
}