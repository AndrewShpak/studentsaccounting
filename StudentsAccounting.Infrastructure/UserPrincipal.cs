using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using StudentsAccounting.Application.Interfaces;

namespace StudentsAccounting.Infrastructure
{
    public class UserPrincipal : IUserPrincipal
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserPrincipal(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }


        public string Id => GetUserId();


        private string GetUserId()
        {
            return _httpContextAccessor.HttpContext
                .User.FindFirst(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value;
        }
    }
}