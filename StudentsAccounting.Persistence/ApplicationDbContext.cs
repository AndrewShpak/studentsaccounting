using Microsoft.EntityFrameworkCore;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Domain.Entities;
using StudentsAccounting.Persistence.Configurations;

namespace StudentsAccounting.Persistence
{
    public class ApplicationDbContext : DbContext, IDbContext
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Course> Courses { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<UserCourse> UserCourses { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CourseConfiguration).Assembly);
        }
        
    }
}