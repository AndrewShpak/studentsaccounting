﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StudentsAccounting.Persistence.Migrations
{
    public partial class InitializeDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Roles",
                table => new
                {
                    Id = table.Column<string>(),
                    Name = table.Column<string>(maxLength: 200)
                },
                constraints: table => { table.PrimaryKey("PK_Roles", x => x.Id); });

            migrationBuilder.CreateTable(
                "Users",
                table => new
                {
                    Id = table.Column<string>(),
                    Age = table.Column<int>(),
                    Email = table.Column<string>(maxLength: 255),
                    Name = table.Column<string>(maxLength: 200),
                    LastName = table.Column<string>(maxLength: 200),
                    Registered = table.Column<DateTime>(),
                    StudyDate = table.Column<DateTime>(nullable: true),
                    Avatar = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.Id); });

            migrationBuilder.CreateTable(
                "Courses",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 300),
                    ShortName = table.Column<string>(maxLength: 200, nullable: true),
                    OwnerId = table.Column<string>(nullable: true),
                    Registered = table.Column<DateTime>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                    table.ForeignKey(
                        "FK_Courses_Users_OwnerId",
                        x => x.OwnerId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "UserRoles",
                table => new
                {
                    UserId = table.Column<string>(),
                    RoleId = table.Column<string>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new {x.UserId, x.RoleId});
                    table.ForeignKey(
                        "FK_UserRoles_Roles_RoleId",
                        x => x.RoleId,
                        "Roles",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_UserRoles_Users_UserId",
                        x => x.UserId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "UserCourses",
                table => new
                {
                    UserId = table.Column<string>(),
                    CourseId = table.Column<int>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCourses", x => new {x.UserId, x.CourseId});
                    table.ForeignKey(
                        "FK_UserCourses_Courses_CourseId",
                        x => x.CourseId,
                        "Courses",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_UserCourses_Users_UserId",
                        x => x.UserId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                "IX_Courses_OwnerId",
                "Courses",
                "OwnerId");

            migrationBuilder.CreateIndex(
                "IX_UserCourses_CourseId",
                "UserCourses",
                "CourseId");

            migrationBuilder.CreateIndex(
                "IX_UserRoles_RoleId",
                "UserRoles",
                "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "UserCourses");

            migrationBuilder.DropTable(
                "UserRoles");

            migrationBuilder.DropTable(
                "Courses");

            migrationBuilder.DropTable(
                "Roles");

            migrationBuilder.DropTable(
                "Users");
        }
    }
}