using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Persistence.Configurations
{
    public class UserCourseConfiguration : IEntityTypeConfiguration<UserCourse>
    {
        public void Configure(EntityTypeBuilder<UserCourse> builder)
        {
            builder.HasKey(e => new {e.UserId, e.CourseId});

            builder.HasOne(d => d.User)
                .WithMany(d => d.UserCourses)
                .HasForeignKey(d => d.UserId);

            builder.HasOne(d => d.Course)
                .WithMany(p => p.UserCourses)
                .HasForeignKey(d => d.CourseId);
        }
    }
}