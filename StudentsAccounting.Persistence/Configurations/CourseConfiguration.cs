using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Persistence.Configurations
{
    public class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Name).IsRequired().HasMaxLength(300);
            builder.Property(e => e.ShortName).HasMaxLength(200);
            builder.Property(e => e.Registered).IsRequired();
            builder.HasOne(e => e.Owner)
                .WithMany(e => e.Courses)
                .HasForeignKey(e => e.OwnerId);
        }
    }
}