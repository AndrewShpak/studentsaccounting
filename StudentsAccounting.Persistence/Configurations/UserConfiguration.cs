using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Persistence.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Age).IsRequired();
            builder.Property(e => e.Name).IsRequired().HasMaxLength(200);
            builder.Property(e => e.LastName).IsRequired().HasMaxLength(200);
            builder.Property(e => e.Registered).IsRequired();
            builder.Property(e => e.Email).IsRequired().HasMaxLength(255);
            builder.Property(e => e.Avatar).HasMaxLength(500);
            builder.Ignore(e => e.FullName);
        }
    }
}