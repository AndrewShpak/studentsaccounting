using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using StudentsAccounting.Application.Auth.Commands.Register;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Auth.Commands.Login
{
    public class LoginCommand : IRequest<string>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }

        public class LoginCommandHandler : IRequestHandler<LoginCommand, string>
        {
            private readonly IConfiguration _configuration;
            private readonly IDbContext _context;
            private readonly IMapper _mapper;
            private readonly IMediator _mediator;

            public LoginCommandHandler(IMediator mediator, IDbContext context, IConfiguration configuration,
                IMapper mapper)
            {
                _mediator = mediator;
                _context = context;
                _configuration = configuration;
                _mapper = mapper;
            }

            public async Task<string> Handle(LoginCommand request, CancellationToken cancellationToken)
            {
                var entity =
                    await _context.Users
                        .SingleOrDefaultAsync(x => x.Email == request.Email, cancellationToken);
                if (entity == null)
                {
                    var registerCommand = _mapper.Map<RegisterCommand>(request);
                    entity = await _mediator.Send(registerCommand, cancellationToken);
                }


                var userClaims = GetUserClaims(entity);
                var token = GenerateJwtToken(userClaims);
                return token;
            }

            private string GenerateJwtToken(ClaimsIdentity userClaims)
            {
                var options = _configuration.GetSection("Authentication");
                var claims = new List<Claim>();
                var now = DateTime.Now;
                var jwt = new JwtSecurityToken(
                    options["Issuer"],
                    options["Audience"],
                    userClaims.Claims,
                    now,
                    now.Add(TimeSpan.FromHours(Convert.ToDouble(options["LifeTime"]))),
                    new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.ASCII.GetBytes(options["Key"])),
                        SecurityAlgorithms.HmacSha512)
                );
                return new JwtSecurityTokenHandler().WriteToken(jwt);
            }

            private ClaimsIdentity GetUserClaims(User user)
            {
                var roles = GetUserRoles(user);
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, string.Join(",", roles))
                };
                var claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            private IEnumerable<string> GetUserRoles(User user)
            {
                return _context.UserRoles.Where(x => x.UserId == user.Id).Select(x => x.Role.Name);
            }
        }
    }
}