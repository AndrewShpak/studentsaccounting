using FluentValidation;

namespace StudentsAccounting.Application.Auth.Commands.Login
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator()
        {
            RuleFor(x => x.UserName).NotEmpty();
            RuleFor(x => x.Avatar).NotEmpty();
            RuleFor(x => x.Email).NotEmpty();
        }
    }
}