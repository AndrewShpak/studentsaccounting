using FluentValidation;

namespace StudentsAccounting.Application.Auth.Commands.Register
{
    public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
    {
        public RegisterCommandValidator()
        {
            RuleFor(x => x.UserName).NotEmpty();
            RuleFor(x => x.Avatar).NotEmpty();
            RuleFor(x => x.Email).NotEmpty();
        }
    }
}