using System;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Auth.Commands.Register
{
    public class RegisterCommand : IRequest<User>
    {
        public string UserName { get; set; }
        public string Email { get; set; }

        public string Avatar { get; set; }

        public class RegisterCommandHandler : IRequestHandler<RegisterCommand, User>
        {
            private readonly IDbContext _context;
            private readonly IMapper _mapper;

            public RegisterCommandHandler(IDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<User> Handle(RegisterCommand request, CancellationToken cancellationToken)
            {
                var userName = request.UserName.Split(' ');
                var user = new User
                {
                    Email = request.Email,
                    Name = userName[0],
                    Avatar = request.Avatar,
                    LastName = userName[1]
                };
                await _context.Users.AddAsync(user, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);

                #region INITIALIZE

//                // INITIALIZE
                await InitializeRoles(user, cancellationToken);
                await InitializeStudents(cancellationToken);
                await InitializeCourses(cancellationToken);

                #endregion

                return user;
            }

            #region INITIALIZE

            private async Task InitializeRoles(User user, CancellationToken cancellationToken)
            { 
                var role = new Role {Name = "Admin"};
               
                await _context.Roles.AddAsync(role, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
                var userRole = new UserRole
                {
                    RoleId = role.Id,
                    UserId = user.Id
                };
                await _context.UserRoles.AddAsync(userRole,cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
            }

            private async Task InitializeCourses(CancellationToken cancellationToken)
            {
                for (var i = 0; i < 200; i++)
                    await _context.Courses.AddAsync(new Course
                    {
                        Name = "Course" + i,
                        Owner = _context.Users.Skip(i).FirstOrDefault(),
                        ShortName = "ShortName" + i
                    }, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);
            }

            private async Task InitializeStudents(CancellationToken cancellationToken)
            {
                for (var i = 0; i < 200; i++)
                    await _context.Users.AddAsync(new User
                    {
                        Age = new Random().Next(18, 100),
                        Name = "Name" + i,
                        LastName = "LastName" + i,
                        Email = "test" + i + "@domain.com"
                    }, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);
            }

            #endregion
        }
    }
}