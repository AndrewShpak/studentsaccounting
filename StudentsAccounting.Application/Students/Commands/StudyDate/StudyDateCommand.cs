using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using StudentsAccounting.Application.Exceptions;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Application.Notifications.Commands.BatchNotifications;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Students.Commands.StudyDate
{
    public class StudyDateCommand : IRequest
    {
        public string Date { get; set; }
        private DateTime GetDate => Convert.ToDateTime(Date);

        public class SetStudentStudyDateCommandHandler : IRequestHandler<StudyDateCommand>
        {
            private readonly IDbContext _context;
            private readonly IMediator _mediator;
            private readonly IUserPrincipal _userPrincipal;

            public SetStudentStudyDateCommandHandler(IMediator mediator, IDbContext context,
                IUserPrincipal userPrincipal)
            {
                _mediator = mediator;
                _context = context;
                _userPrincipal = userPrincipal;
            }

            public async Task<Unit> Handle(StudyDateCommand request, CancellationToken cancellationToken)
            {
                var entity =
                    await _context.Users.FirstOrDefaultAsync(x => x.Id == _userPrincipal.Id, cancellationToken);

                if (entity == null) throw new NotFoundException(nameof(User), _userPrincipal.Id);

                entity.StudyDate = request.GetDate;
                _context.Users.Update(entity);
                await _context.SaveChangesAsync(cancellationToken);

                await _mediator.Publish(new BatchNotificationsCommand
                {
                    DateTimes = GetEmailNotificationsDateTimes(request.GetDate),
                    ToUserName = entity.FullName,
                    ToEmail = entity.Email,
                    Subject = "Study Date Notification",
                    Body =
                        $"Dear {entity.FullName}. \n We remind you that you have begun studying on {request.GetDate.ToShortDateString()}. \n\n with best regards, \n service Students Accounting"
                }, cancellationToken);

                return Unit.Value;
            }

            private static IEnumerable<DateTime> GetEmailNotificationsDateTimes(DateTime requestDate)
            {
                var prevDate = requestDate.AddDays(-1);
                yield return requestDate.AddDays(-30);
                yield return requestDate.AddDays(-7);
                yield return new DateTime(prevDate.Year, prevDate.Month, prevDate.Day, 8, 0, 0);
            }
        }
    }
}