using FluentValidation;

namespace StudentsAccounting.Application.Students.Commands.StudyDate
{
    public class StudyDateCommandValidator : AbstractValidator<StudyDateCommand>
    {
        public StudyDateCommandValidator()
        {
            RuleFor(r => r.Date).NotEmpty();
        }
    }
}