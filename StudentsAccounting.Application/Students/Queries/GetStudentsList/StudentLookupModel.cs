using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using AutoMapper;
using StudentsAccounting.Application.Courses.Queries;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Application.Interfaces.Mapping;
using StudentsAccounting.Application.Users.Queries.UserData;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Students.Queries.GetStudentsList
{
    public class StudentLookupModel : ICustomMapping
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string Registered { get; set; }
        public string StudyDate { get; set; }
        public IEnumerable<string> Roles { get; set; }
        
        public bool IsAdmin { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<User, StudentLookupModel>()
                .ForMember(f => f.Roles, opt => opt.MapFrom<RolesResolver>())
                .ForMember(f => f.Registered, opt => opt.MapFrom(c => c.Registered.ToShortDateString()))
                .ForMember(f => f.StudyDate,
                    opt => opt.MapFrom(c =>
                        c.StudyDate.HasValue ? c.StudyDate.Value.ToShortDateString() : string.Empty));
        }
        
    }

  

    internal class RolesResolver : IValueResolver<User, StudentLookupModel, IEnumerable<string>>
    {
        private readonly IDbContext _context;

        public RolesResolver(IDbContext context)
        {
            _context = context;
        }

        public IEnumerable<string> Resolve(User source, StudentLookupModel destination, IEnumerable<string> destMember, ResolutionContext context)
        {
            return _context.UserRoles.Where(x => x.UserId == source.Id).Select(x => x.Role.Id);
        }
    }
}