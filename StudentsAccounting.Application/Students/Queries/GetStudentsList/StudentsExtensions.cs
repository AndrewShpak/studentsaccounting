using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace StudentsAccounting.Application.Students.Queries.GetStudentsList
{
    public static class StudentsExtensions
    {
        public static IQueryable<TEntity> Filter<TEntity>(this IQueryable<TEntity> source, string column,
            string value)
        {
            return string.IsNullOrEmpty(value) ? source :
                string.IsNullOrEmpty(column) ? source : source.Where($"{column}.ToString().ToLower().Contains(\"{value}\")");
        }

        public static IQueryable<TEntity> Filter<TEntity>(this IQueryable<TEntity> source,
            IEnumerable<KeyValuePair<string, List<string>>> filters)
        {
            return filters.Any()
                ? filters.Aggregate(source,
                    (current, filter) => current.Filter(filter.Key.FirstCharToUpper(), filter.Value.FirstOrDefault()))
                : source;
        }

        public static IQueryable<TEntity> Sort<TEntity>(this IQueryable<TEntity> source, string column,
            bool isDescSorting)
        {
            if (string.IsNullOrEmpty(column))
                return source;
            var direction = isDescSorting ? "DESC" : string.Empty;
            return source.OrderBy($"{column} {direction}");
        }

        public static string FirstCharToUpper(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            var a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}