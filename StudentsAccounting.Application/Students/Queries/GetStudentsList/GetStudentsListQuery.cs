using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Application.Roles.Commands.UpdateUserRoles;
using StudentsAccounting.Application.Users.Queries.UserData;

namespace StudentsAccounting.Application.Students.Queries.GetStudentsList
{
    public class GetStudentsListQuery : IRequest<StudentsListViewModel>
    {
        public int Current { get; set; } = 1;
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public string Filters { get; set; }

        public Dictionary<string,List<string>> GetFilters()
        {
            return string.IsNullOrEmpty(Filters) || Filters.Length == 2
                ? new Dictionary<string, List<string>>()
                : JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(Filters);
        }

        private bool IsDescSorting()
        {
            return SortOrder == "descend";
        }


        public class GetStudentsListQueryHandle : IRequestHandler<GetStudentsListQuery, StudentsListViewModel>
        {
            private readonly IDbContext _context;
            private readonly IMapper _mapper;

            public GetStudentsListQueryHandle(IMapper mapper, IDbContext context)
            {
                _mapper = mapper;
                _context = context;
            }

            public async Task<StudentsListViewModel> Handle(GetStudentsListQuery request,
                CancellationToken cancellationToken)
            {
                var students = _context.Users.Filter(request.GetFilters().Where(x=> x.Value.Any()))
                        .Sort(request.SortField.FirstCharToUpper(), request.IsDescSorting())
                    ;

                var studentsListViewModel = new StudentsListViewModel
                {
                    Students = _mapper.Map<List<StudentLookupModel>>(students.Skip((request.Current - 1) * 10).Take(10)
                        ),
                    Pagination = new Pagination
                    {
                        Current = request.Current, Total = await students.CountAsync(cancellationToken)
                    },
                    Roles = _mapper.Map<List<RoleViewModel>>(_context.Roles)
                };
                return studentsListViewModel;
            }
        }
    }
}