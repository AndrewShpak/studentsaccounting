using System.Collections.Generic;
using StudentsAccounting.Application.Roles.Commands.UpdateUserRoles;
using StudentsAccounting.Application.Users.Queries.UserData;

namespace StudentsAccounting.Application.Students.Queries.GetStudentsList
{
    public class StudentsListViewModel
    {
        public Pagination Pagination { get; set; }
        public Dictionary<string, List<string>> Filters { get; set; }
        public List<StudentLookupModel> Students { get; set; }
        public List<RoleViewModel> Roles { get; set; }
    }

    public class Pagination
    {
        public int Total { get; set; }
        public int Current { get; set; }
    }
}