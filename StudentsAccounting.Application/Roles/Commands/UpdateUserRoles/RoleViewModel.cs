namespace StudentsAccounting.Application.Roles.Commands.UpdateUserRoles
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}