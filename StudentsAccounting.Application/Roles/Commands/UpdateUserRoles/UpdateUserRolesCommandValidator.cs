using FluentValidation;

namespace StudentsAccounting.Application.Roles.Commands.UpdateUserRoles
{
    public class UpdateUserRolesCommandValidator:AbstractValidator<UpdateUserRolesCommand>
    {
        public UpdateUserRolesCommandValidator()
        {
            RuleFor(f => f.UserId).NotEmpty();
            RuleFor(f => f.Roles).NotNull();
        }
    }
}