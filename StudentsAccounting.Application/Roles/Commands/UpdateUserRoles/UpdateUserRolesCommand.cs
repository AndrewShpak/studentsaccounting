using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Roles.Commands.UpdateUserRoles
{
    public class UpdateUserRolesCommand : IRequest
    {
        public List<string> Roles { get; set; } 
        public string UserId { get; set; }
        
        public class UpdateUserRolesCommandHandler:IRequestHandler<UpdateUserRolesCommand>
        {
            private readonly IDbContext _context;

            public UpdateUserRolesCommandHandler(IDbContext context)
            {
                _context = context;
            }


            public async Task<Unit> Handle(UpdateUserRolesCommand request, CancellationToken cancellationToken)
            {
                _context.UserRoles.RemoveRange( _context.UserRoles.Where(x => x.UserId == request.UserId));
                foreach (var role in request.Roles)
                {
                    await _context.UserRoles.AddAsync(new UserRole
                    {
                        RoleId = role,
                        UserId = request.UserId
                    }, cancellationToken);
                }
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}