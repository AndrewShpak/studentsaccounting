using System.Threading;
using System.Threading.Tasks;
using MediatR;
using StudentsAccounting.Application.Exceptions;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Courses.Commands.DeleteCourse
{
    public class DeleteCourseCommand : IRequest
    {
        public int Id { get; set; }

        public class DeleteCourseCommandHandler : IRequestHandler<DeleteCourseCommand>
        {
            private readonly IDbContext _context;

            public DeleteCourseCommandHandler(IDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteCourseCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Courses.FindAsync(request.Id);
                if (entity == null) throw new NotFoundException(nameof(Course), request.Id);

                _context.Courses.Remove(entity);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}