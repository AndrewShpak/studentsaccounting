using FluentValidation;

namespace StudentsAccounting.Application.Courses.Commands.DeleteCourse
{
    public class DeleteCourseCommandValidator : AbstractValidator<DeleteCourseCommand>
    {
        public DeleteCourseCommandValidator()
        {
            RuleFor(r => r.Id).NotEmpty();
        }
    }
}