using System.Threading;
using System.Threading.Tasks;
using MediatR;
using StudentsAccounting.Application.Interfaces;

namespace StudentsAccounting.Application.Courses.Commands.Unsubscribe
{
    public class UnsubscribeCommand : IRequest
    {
        public string UserId { get; set; }
        public int CourseId { get; set; }

        public class UnsubscribeCommandHandler : IRequestHandler<UnsubscribeCommand>
        {
            private readonly IDbContext _context;

            public UnsubscribeCommandHandler(IDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UnsubscribeCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.UserCourses.FindAsync(request.UserId, request.CourseId);
                _context.UserCourses.Remove(entity);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}