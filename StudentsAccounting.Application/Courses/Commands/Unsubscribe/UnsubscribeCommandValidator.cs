using FluentValidation;

namespace StudentsAccounting.Application.Courses.Commands.Unsubscribe
{
    public class UnsubscribeCommandValidator : AbstractValidator<UnsubscribeCommand>
    {
        public UnsubscribeCommandValidator()
        {
            RuleFor(r => r.CourseId).NotEmpty();
            RuleFor(r => r.UserId).NotEmpty();
        }
    }
}