using FluentValidation;

namespace StudentsAccounting.Application.Courses.Commands.UpdateCourse
{
    public class UpdateCourseCommandValidator : AbstractValidator<UpdateCourseCommand>
    {
        public UpdateCourseCommandValidator()
        {
            RuleFor(r => r.Id).NotEmpty();
            RuleFor(r => r.Name).NotEmpty().MaximumLength(300);
            RuleFor(r => r.ShortName).MaximumLength(200);
        }
    }
}