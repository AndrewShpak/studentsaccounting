using System.Threading;
using System.Threading.Tasks;
using MediatR;
using StudentsAccounting.Application.Exceptions;
using StudentsAccounting.Application.Interfaces;

namespace StudentsAccounting.Application.Courses.Commands.UpdateCourse
{
    public class UpdateCourseCommand : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }

        public class UpdateCourseCommandHandler : IRequestHandler<UpdateCourseCommand>
        {
            private readonly IDbContext _context;

            public UpdateCourseCommandHandler(IDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateCourseCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Courses.FindAsync(request.Id);
                if (entity == null) throw new NotFoundException(nameof(entity), request.Id);

                entity.Name = request.Name;
                entity.ShortName = request.ShortName;

                _context.Courses.Update(entity);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}