using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using StudentsAccounting.Application.Courses.Commands.Unsubscribe;
using StudentsAccounting.Application.Exceptions;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Courses.Commands.Subscribe
{
    public class SubscribeCommand : IRequest
    {
        public int Id { get; set; }

        public class SubscribeCommandHandler : IRequestHandler<SubscribeCommand>
        {
            private readonly IDbContext _context;
            private readonly IMediator _mediator;
            private readonly IUserPrincipal _user;

            public SubscribeCommandHandler(IDbContext context, IUserPrincipal user, IMediator mediator)
            {
                _context = context;
                _user = user;
                _mediator = mediator;
            }

            public async Task<Unit> Handle(SubscribeCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Courses
                    .FindAsync(request.Id);

                if (entity == null) throw new NotFoundException(nameof(Course), request.Id);

                var userId = _user.Id;

                if (string.IsNullOrEmpty(userId)) throw new NotFoundException(nameof(User), userId);
                if (await _context.UserCourses.AnyAsync(x => x.UserId == userId && x.CourseId == entity.Id,
                    cancellationToken))
                {
                    await _mediator.Send(new UnsubscribeCommand {UserId = userId, CourseId = entity.Id},
                        cancellationToken);
                    return Unit.Value;
                }


                await _context.UserCourses.AddAsync(new UserCourse
                {
                    Course = entity,
                    User = await _context.Users.FindAsync(userId)
                }, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}