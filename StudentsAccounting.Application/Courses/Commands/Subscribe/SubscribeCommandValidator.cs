using FluentValidation;

namespace StudentsAccounting.Application.Courses.Commands.Subscribe
{
    public class SubscribeCommandValidator : AbstractValidator<SubscribeCommand>
    {
        public SubscribeCommandValidator()
        {
            RuleFor(r => r.Id).NotEmpty();
        }
    }
}