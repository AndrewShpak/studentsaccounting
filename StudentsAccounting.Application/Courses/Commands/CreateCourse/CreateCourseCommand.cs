using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using StudentsAccounting.Application.Courses.Queries;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Courses.Commands.CreateCourse
{
    public class CreateCourseCommand : IRequest<CourseViewModel>
    {
        public string Name { get; set; }
        public string ShortName { get; set; }

        public class CreateCourseCommandHandler : IRequestHandler<CreateCourseCommand, CourseViewModel>
        {
            private readonly IDbContext _context;
            private readonly IMapper _mapper;
            private readonly IUserPrincipal _userPrincipal;

            public CreateCourseCommandHandler(IDbContext context, IMapper mapper, IUserPrincipal userPrincipal)
            {
                _context = context;
                _mapper = mapper;
                _userPrincipal = userPrincipal;
            }

            public async Task<CourseViewModel> Handle(CreateCourseCommand request, CancellationToken cancellationToken)
            {
                var entity = new Course
                {
                    Name = request.Name,
                    ShortName = request.ShortName,
                    Owner = await _context.Users.FindAsync(_userPrincipal.Id)
                };
                await _context.Courses.AddAsync(entity, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
                return _mapper.Map<CourseViewModel>(entity);
            }
        }
    }
}