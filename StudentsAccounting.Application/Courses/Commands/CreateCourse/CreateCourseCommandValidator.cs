using FluentValidation;

namespace StudentsAccounting.Application.Courses.Commands.CreateCourse
{
    public class CreateCourseCommandValidator : AbstractValidator<CreateCourseCommand>
    {
        public CreateCourseCommandValidator()
        {
            RuleFor(r => r.Name).NotEmpty().MaximumLength(300);
            RuleFor(r => r.ShortName).MaximumLength(200);
        }
    }
}