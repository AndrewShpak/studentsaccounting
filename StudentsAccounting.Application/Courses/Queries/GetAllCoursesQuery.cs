using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using StudentsAccounting.Application.Interfaces;

namespace StudentsAccounting.Application.Courses.Queries
{
    public class GetAllCoursesQuery : IRequest<List<CourseViewModel>>
    {
        public string UserId { get; set; }

        public class GetAllCoursesQueryHandler : IRequestHandler<GetAllCoursesQuery, List<CourseViewModel>>
        {
            private readonly IDbContext _context;
            private readonly IMapper _mapper;

            public GetAllCoursesQueryHandler(IDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<CourseViewModel>> Handle(GetAllCoursesQuery request,
                CancellationToken cancellationToken)
            {

               return await Task.FromResult(_mapper.Map<List<CourseViewModel>>(_context.Courses.OrderBy(p => p.Name)
                    .Include(x => x.Owner)
                    .Include(x => x.UserCourses).OrderBy(x => x.Name)));
            }
        }
    }
}