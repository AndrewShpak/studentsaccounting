using System.Linq;
using AutoMapper;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Application.Interfaces.Mapping;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Courses.Queries
{
    public class CourseViewModel : ICustomMapping
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public bool IsRegistered { get; set; }
        public bool EditEnabled { get; set; }
        public bool DeleteEnabled { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Course, CourseViewModel>()
                .ForMember(f => f.IsRegistered, opt => opt.MapFrom<RegistrationStatusResolver>())
                .ForMember(f => f.EditEnabled, opt => opt.MapFrom<PermissionsResolver>())
                .ForMember(f => f.DeleteEnabled, opt => opt.MapFrom<PermissionsResolver>());
        }


        internal class PermissionsResolver : IValueResolver<Course, CourseViewModel, bool>
        {
            private readonly IUserPrincipal _userPrincipal;

            public PermissionsResolver(IUserPrincipal userPrincipal)
            {
                _userPrincipal = userPrincipal;
            }

            public bool Resolve(Course source, CourseViewModel destination, bool destMember, ResolutionContext context)
            {
                return _userPrincipal.Id == source.OwnerId;
            }
        }

        internal class RegistrationStatusResolver : IValueResolver<Course, CourseViewModel, bool>
        {
            private readonly IUserPrincipal _userPrincipal;

            public RegistrationStatusResolver(IUserPrincipal userPrincipal)
            {
                _userPrincipal = userPrincipal;
            }

            public bool Resolve(Course source, CourseViewModel destination, bool destMember, ResolutionContext context)
            {
                return source.UserCourses.Any(x => x.UserId == _userPrincipal.Id);
            }
        }
    }
}