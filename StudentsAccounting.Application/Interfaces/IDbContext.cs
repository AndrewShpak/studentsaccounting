using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Interfaces
{
    public interface IDbContext
    {
        DbSet<Role> Roles { get; set; }

        DbSet<User> Users { get; set; }

        DbSet<Course> Courses { get; set; }

        DbSet<UserCourse> UserCourses { get; set; }

        DbSet<UserRole> UserRoles { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}