namespace StudentsAccounting.Application.Interfaces.Email
{
    public interface IEmailService
    {
        void Send(Message message);
    }
}