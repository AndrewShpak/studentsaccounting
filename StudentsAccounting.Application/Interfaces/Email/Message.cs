namespace StudentsAccounting.Application.Interfaces.Email
{
    public class Message
    {
        public string ToEmail { get; set; }
        public string ToUserName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}