namespace StudentsAccounting.Application.Interfaces
{
    public interface IUserPrincipal
    {
        string Id { get; }
    }
}