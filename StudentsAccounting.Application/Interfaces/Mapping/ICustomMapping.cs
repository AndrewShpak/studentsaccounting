using AutoMapper;

namespace StudentsAccounting.Application.Interfaces.Mapping
{
    public interface ICustomMapping
    {
        void CreateMappings(Profile configuration);
    }
}