using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json.Serialization;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Application.Roles.Commands.UpdateUserRoles;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Users.Commands.UpdateUser
{
    public class UpdateUserCommand : IRequest
    {
        public string Id { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        
        public List<string> Roles { get; set; } 

        public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand>
        {
            private readonly IDbContext _context;
            private readonly IMediator _mediator;

            public UpdateUserCommandHandler(IDbContext context, IMediator mediator)
            {
                _context = context;
                _mediator = mediator;
            }

            public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Users.FindAsync(request.Id);
                entity.Name = request.Name;
                entity.LastName = request.LastName;
                entity.Age = request.Age;
                _context.Users.Update(entity);
                await _context.SaveChangesAsync(cancellationToken);
                if (request.Roles!=null)
                {
                    await _mediator.Send(new UpdateUserRolesCommand
                    {
                        Roles = request.Roles,
                        UserId = request.Id
                    }, cancellationToken);
                }
                return Unit.Value;
            }
        }
    }
}