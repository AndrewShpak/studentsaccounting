using FluentValidation;

namespace StudentsAccounting.Application.Users.Commands.UpdateUser
{
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(200);
            RuleFor(x => x.LastName).NotEmpty().MaximumLength(200);
            RuleFor(x => x.Age).NotEmpty().GreaterThan(1);
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}