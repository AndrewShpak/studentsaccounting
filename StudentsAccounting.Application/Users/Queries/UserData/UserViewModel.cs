using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Newtonsoft.Json;
using StudentsAccounting.Application.Interfaces.Mapping;
using StudentsAccounting.Application.Roles.Commands.UpdateUserRoles;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Users.Queries.UserData
{
    public class UserViewModel : ICustomMapping
    {
        [JsonProperty("access_token", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Token { get; set; }

        public string UserName { get; set; }

        public string Id { get; set; }

        public string Avatar { get; set; }

        public bool HasStudyDate { get; set; }

        public int Age { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Registered { get; set; }

        public string StudyDate { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public IEnumerable<RoleViewModel> UserRoles { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<User, UserViewModel>()
                .ForMember(f => f.Roles, opt => opt.Ignore())
                .ForMember(f => f.UserRoles, opt => opt.Ignore())
                .ForMember(f => f.Registered, opt => opt.MapFrom(c => c.Registered.ToShortDateString()))
                .ForMember(f => f.UserName, opt => opt.MapFrom(c => $"{c.Name} {c.LastName}"))
                .ForMember(f => f.StudyDate,
                    opt => opt.MapFrom(c =>
                        c.StudyDate.HasValue ? c.StudyDate.Value.ToShortDateString() : string.Empty))
                .ForMember(f => f.HasStudyDate,
                    opt => opt.MapFrom(c => c.StudyDate != null));
        }
    }
}