using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using StudentsAccounting.Application.Exceptions;
using StudentsAccounting.Application.Interfaces;
using StudentsAccounting.Application.Roles.Commands.UpdateUserRoles;
using StudentsAccounting.Domain.Entities;

namespace StudentsAccounting.Application.Users.Queries.UserData
{
    public class UserDataQuery : IRequest<UserViewModel>
    {
        public class UserDataQueryHandler : IRequestHandler<UserDataQuery, UserViewModel>
        {
            private readonly IDbContext _context;
            private readonly IMapper _mapper;
            private readonly IUserPrincipal _user;

            public UserDataQueryHandler(IDbContext context, IUserPrincipal user, IMapper mapper)
            {
                _context = context;
                _user = user;
                _mapper = mapper;
            }

            public async Task<UserViewModel> Handle(UserDataQuery request, CancellationToken cancellationToken)
            {
                var entity = await _context.Users.Include(x => x.UserRoles)
                    .FirstOrDefaultAsync(x => x.Id == _user.Id, cancellationToken);
                if (entity == null) throw new NotFoundException(nameof(User), _user.Id);
                
                var viewModel = _mapper.Map<UserViewModel>(entity);
                var roles = _context.UserRoles.Where(x => x.UserId == entity.Id).Select(x => x.Role);
                viewModel.Roles = roles.Select(x=> x.Name) ;
                viewModel.UserRoles = _mapper.Map<List<RoleViewModel>>(roles);
                
                return viewModel;
            }

           
        }
    }
}