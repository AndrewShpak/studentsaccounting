using FluentValidation;

namespace StudentsAccounting.Application.Notifications.Commands.Notify
{
    public class NotifyCommandValidator : AbstractValidator<NotifyCommand>
    {
        public NotifyCommandValidator()
        {
            RuleFor(r => r.Date).NotEmpty();
            RuleFor(r => r.Body).NotEmpty();
            RuleFor(r => r.Subject).NotEmpty();
            RuleFor(r => r.ToEmail).NotEmpty();
            RuleFor(r => r.ToUserName).NotEmpty();
        }
    }
}