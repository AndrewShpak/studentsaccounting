using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Hangfire;
using MediatR;
using StudentsAccounting.Application.Interfaces.Email;

namespace StudentsAccounting.Application.Notifications.Commands.Notify
{
    public class NotifyCommand : INotification
    {
        public DateTime Date { get; set; }
        public string ToEmail { get; set; }
        public string ToUserName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public class NotifyCommandHandler : INotificationHandler<NotifyCommand>
        {
            private readonly IBackgroundJobClient _backgroundJob;
            private readonly IEmailService _emailService;
            private readonly IMapper _mapper;

            public NotifyCommandHandler(IEmailService emailService, IBackgroundJobClient backgroundJob, IMapper mapper)
            {
                _emailService = emailService;
                _backgroundJob = backgroundJob;
                _mapper = mapper;
            }

            public Task Handle(NotifyCommand notification, CancellationToken cancellationToken)
            {
                _backgroundJob.Schedule(
                    () => _emailService.Send(_mapper.Map<Message>(notification)),
                    notification.Date);
                return Unit.Task;
            }
        }
    }
}