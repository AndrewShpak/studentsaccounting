using FluentValidation;

namespace StudentsAccounting.Application.Notifications.Commands.BatchNotifications
{
    public class BatchNotificationsCommandValidator : AbstractValidator<BatchNotificationsCommand>
    {
        public BatchNotificationsCommandValidator()
        {
            RuleFor(r => r.DateTimes).NotEmpty();
            RuleFor(r => r.Body).NotEmpty();
            RuleFor(r => r.Subject).NotEmpty();
            RuleFor(r => r.ToEmail).NotEmpty();
            RuleFor(r => r.ToUserName).NotEmpty();
        }
    }
}