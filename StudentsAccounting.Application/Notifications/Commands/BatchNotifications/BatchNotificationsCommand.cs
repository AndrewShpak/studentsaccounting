using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using StudentsAccounting.Application.Notifications.Commands.Notify;

namespace StudentsAccounting.Application.Notifications.Commands.BatchNotifications
{
    public class BatchNotificationsCommand : INotification
    {
        public IEnumerable<DateTime> DateTimes { get; set; }
        public string ToEmail { get; set; }
        public string ToUserName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public class BatchNotificationsCommandHandler : INotificationHandler<BatchNotificationsCommand>
        {
            private readonly IMapper _mapper;
            private readonly IMediator _mediator;

            public BatchNotificationsCommandHandler(IMediator mediator, IMapper mapper)
            {
                _mediator = mediator;
                _mapper = mapper;
            }

            public async Task Handle(BatchNotificationsCommand notification, CancellationToken cancellationToken)
            {
                foreach (var date in notification.DateTimes)
                    await _mediator.Publish(new NotifyCommand
                    {
                        Date = date,
                        Body = notification.Body,
                        Subject = notification.Subject,
                        ToUserName = notification.ToUserName,
                        ToEmail = notification.ToEmail
                    }, cancellationToken);
            }
        }
    }
}