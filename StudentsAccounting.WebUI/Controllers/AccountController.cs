using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using StudentsAccounting.Application.Auth.Commands.Login;

namespace StudentsAccounting.WebUI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
    }
}