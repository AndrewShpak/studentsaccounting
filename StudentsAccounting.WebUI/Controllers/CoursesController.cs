using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentsAccounting.Application.Courses.Commands.CreateCourse;
using StudentsAccounting.Application.Courses.Commands.DeleteCourse;
using StudentsAccounting.Application.Courses.Commands.Subscribe;
using StudentsAccounting.Application.Courses.Commands.UpdateCourse;
using StudentsAccounting.Application.Courses.Queries;

namespace StudentsAccounting.WebUI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    [Authorize]
    public class CoursesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CoursesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("courses")]
        [ProducesResponseType(typeof(List<CourseViewModel>), (int) HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllCourses()
        {
            return Ok(await _mediator.Send(new GetAllCoursesQuery()));
        }

        [HttpPost]
        [Route("course/subscribe")]
        public async Task<IActionResult> SubscribeToCourse(SubscribeCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpPost]
        [Route("course/create")]
        public async Task<IActionResult> CreateCourse(CreateCourseCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpPut]
        [Route("course/edit")]
        public async Task<IActionResult> UpdateCourse(UpdateCourseCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpDelete]
        [Route("course/delete")]
        public async Task<IActionResult> DeleteCourse([FromQuery] DeleteCourseCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
    }
}