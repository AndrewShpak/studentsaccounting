using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentsAccounting.Application.Students.Commands.StudyDate;
using StudentsAccounting.Application.Students.Queries.GetStudentsList;
using StudentsAccounting.Application.Users.Commands.UpdateUser;

namespace StudentsAccounting.WebUI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public StudentsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET
        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("students")]
        [ProducesResponseType(typeof(StudentsListViewModel), (int) HttpStatusCode.OK)]
        public async Task<IActionResult> GetStudents([FromQuery] GetStudentsListQuery query)
        {
            return Ok(await _mediator.Send(query));
        }

        [HttpPut]
        [Authorize(Roles = "Admin")]
        [Route("user/edit")]
        public async Task<IActionResult>EditStudents(UpdateUserCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpPost]
        [Authorize]
        [Route("study-year")]
        public async Task<IActionResult> SetStudentStudyYear(StudyDateCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
    }
}