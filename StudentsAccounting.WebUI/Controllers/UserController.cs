using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentsAccounting.Application.Users.Queries.UserData;

namespace StudentsAccounting.WebUI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [HttpGet]
        [Route("userData")]
        public async Task<IActionResult> GetUserData()
        {
            return Ok(await _mediator.Send(new UserDataQuery()));
        }
    }
}